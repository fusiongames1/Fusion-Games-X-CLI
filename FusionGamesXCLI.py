# start CLI backend

import os  # command runner
import time  # TIME
import platform  # os finder

# run command: os.system("ls -l")
crunning = int("0")
crun1 = int("0")
Main = "Main"
Hosting = "Hosting"
plat = platform.system()


def get():
    crunning = int("1")
    q2 = str(input("Branch? (case sensitive)"))
    if q2 == "Main":
        print("Cloning from main branch.")
        if plat == "Windows":
            os.system("git clone -b main https://gitlab.com/fusiongames1/fusion-games-x.git")
        elif plat == "Linux":
            os.system("sudo git clone -b main https://gitlab.com/fusiongames1/fusion-games-x.git")
        else:
            print("Error: no os file found, cannot clone.")
    elif q2 == "Hosting":

        print("Cloning hosting branch. (for hosting locally)")
        if plat == "Linux":
            os.system("sudo git clone -b hostingproviders https://gitlab.com/fusiongames1/fusion-games-x.git")
        elif plat == "Windows":
            os.system("git clone -b hostingproviders https://gitlab.com/fusiongames1/fusion-games-x.git")
        else:
            print("Error: no os file found, cannot clone.")
    else:
        print("Error: not reconized branch")
    crunning = int("0")


def update():
    os.system("git pull")


def uninstall():
    print(
        "Warning, this command uninstalls all of the files in the current directory. This includes this python file. "
        "After the command finishes, you might get a error that tells file not found this means that it worked. "
        "thanks for using Fusion Games X")
    q3 = str(input("Are you sure you want to delete Fusion Games X and all of its files? (y/n)"))
    if q3 == "y":
        os.system("rm -r *")
        print("Fusion Games X has been successfully deleted.")
    else:
        exit()


def command():
    q1 = str(input())
    if q1 == "get":
        get()
    elif q1 == "update":
        update()
    elif q1 == "uninstall":
        uninstall()
    elif q1 == "exit":
        end()
    else:
        print("Error: not recognized command")


def end():
    exit()


# start frontend

print("welcome to the Fusion Games X ClI ")
print("version 0.20")

print("Platform: " + plat)

command()
command()
command()
command()
command()
command()
command()
command()
command()
command()
