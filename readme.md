<h1>Fusion Games X CLI readme and documetation</h1>

<p>Download latest version <a href="https://gitlab.com/fusiongames1/Fusion-Games-X-CLI/-/releases">here</a></p>

<p>With fusion Games X CLI, you can manage your version of Fusion Games X with ease</p>

<h2>Docs</h2>

<p>Fusion Games X CLI is based off <a href="https://www.python.org/">Python</a> .<p>

<h3>How to run</h3>
<h4>Requirements</h4>
<p>Latest version of <a href="https://www.python.org/">Python</a></p>
<p>Latest version of <a href="https://git-scm.com/downloads">Git</a></p>
<h3>command list</h3>
<p>this section is the lists of commands that can be run on the CLI</p>

<h4>"get" command</h4>

<p>The "get" command fetches the latest verion of fgx with the branch you choose. (Windows and Linux)</p>


<h4>"update" command</h4>

<p>The update command is curently in beta, but it is for updating your repository to the lastest version of your branch.</p>

<h4>"uninstall" command</h4>

<p>The "uninstall" command uninstalls the Fusion Games X CLI, and if you had previously ran the command "get", it would delete Fusion Games X too. (Windows and Linux)</p>

<h4>"end" command</h4>

<p>The "exit" command ends your session of Fusion Games X CLI. (this command works on Windows and Linux)</p>
